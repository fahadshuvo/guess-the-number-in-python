# Guess the number
import random
num = random.randint(1,100)
print('A Random number has been taken between 1 to 100.\nYou have to guess the number')
i = int(input('How many times you want to try to guess the number : '))
while i <= 0 :
    print('Please type a non zero number or positive number.')
    i = int(input('How many times you want to try to guess the number : '))

count = 1
isGreater = False

while count <=i :
    if count == 1 :
        guessNum = int(input('Try to guess the number : '))
    elif isGreater :
        guessNum = int(input('Try a smaller number : '))
    else :
        guessNum = int(input('Try a greater number : '))

    if guessNum == num :
        print('Congratulations, You guessed the number in',count,'try')
        break
    elif (guessNum > num):
        print('Your number is greater than our Number')
        isGreater = True
    else :
        print('Your number is less than our Number')
        isGreater = False
    count += 1
else:
    if count > i :
        print('You Failed to guess the number')
        print(f'The number was {num}')